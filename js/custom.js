// viewport size
function viewport(){var a=window,b="inner";return"innerWidth"in window||(b="client",a=document.documentElement||document.body),{width:a[b+"Width"],height:a[b+"Height"]}}
var winW=viewport().width;
// viewport size

/*----------begin doc ready----------*/
$(document).ready(function(){

// ios fix
/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)&&$("body").addClass("ios");
// ios fix

// ie fix
var ms_ie = false;
var ua = window.navigator.userAgent;
var old_ie = ua.indexOf('MSIE ');
var new_ie = ua.indexOf('Trident/');
if ((old_ie > -1) || (new_ie > -1)) {
	ms_ie = true;
}
if ( ms_ie ) {
	$("body").addClass("ie");
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a("object"==typeof module&&module.exports?require("jquery"):jQuery)}(function(a){function b(b){var c={},d=/^jQuery\d+$/;return a.each(b.attributes,function(a,b){b.specified&&!d.test(b.name)&&(c[b.name]=b.value)}),c}function c(b,c){var d=this,f=a(this);if(d.value===f.attr(h?"placeholder-x":"placeholder")&&f.hasClass(n.customClass))if(d.value="",f.removeClass(n.customClass),f.data("placeholder-password")){if(f=f.hide().nextAll('input[type="password"]:first').show().attr("id",f.removeAttr("id").data("placeholder-id")),b===!0)return f[0].value=c,c;f.focus()}else d==e()&&d.select()}function d(d){var e,f=this,g=a(this),i=f.id;if(!d||"blur"!==d.type||!g.hasClass(n.customClass))if(""===f.value){if("password"===f.type){if(!g.data("placeholder-textinput")){try{e=g.clone().prop({type:"text"})}catch(j){e=a("<input>").attr(a.extend(b(this),{type:"text"}))}e.removeAttr("name").data({"placeholder-enabled":!0,"placeholder-password":g,"placeholder-id":i}).bind("focus.placeholder",c),g.data({"placeholder-textinput":e,"placeholder-id":i}).before(e)}f.value="",g=g.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id",g.data("placeholder-id")).show()}else{var k=g.data("placeholder-password");k&&(k[0].value="",g.attr("id",g.data("placeholder-id")).show().nextAll('input[type="password"]:last').hide().removeAttr("id"))}g.addClass(n.customClass),g[0].value=g.attr(h?"placeholder-x":"placeholder")}else g.removeClass(n.customClass)}function e(){try{return document.activeElement}catch(a){}}var f,g,h=!1,i="[object OperaMini]"===Object.prototype.toString.call(window.operamini),j="placeholder"in document.createElement("input")&&!i&&!h,k="placeholder"in document.createElement("textarea")&&!i&&!h,l=a.valHooks,m=a.propHooks,n={};j&&k?(g=a.fn.placeholder=function(){return this},g.input=!0,g.textarea=!0):(g=a.fn.placeholder=function(b){var e={customClass:"placeholder"};return n=a.extend({},e,b),this.filter((j?"textarea":":input")+"["+(h?"placeholder-x":"placeholder")+"]").not("."+n.customClass).not(":radio, :checkbox, [type=hidden]").bind({"focus.placeholder":c,"blur.placeholder":d}).data("placeholder-enabled",!0).trigger("blur.placeholder")},g.input=j,g.textarea=k,f={get:function(b){var c=a(b),d=c.data("placeholder-password");return d?d[0].value:c.data("placeholder-enabled")&&c.hasClass(n.customClass)?"":b.value},set:function(b,f){var g,h,i=a(b);return""!==f&&(g=i.data("placeholder-textinput"),h=i.data("placeholder-password"),g?(c.call(g[0],!0,f)||(b.value=f),g[0].value=f):h&&(c.call(b,!0,f)||(h[0].value=f),b.value=f)),i.data("placeholder-enabled")?(""===f?(b.value=f,b!=e()&&d.call(b)):(i.hasClass(n.customClass)&&c.call(b),b.value=f),i):(b.value=f,i)}},j||(l.input=f,m.value=f),k||(l.textarea=f,m.value=f),a(function(){a(document).delegate("form","submit.placeholder",function(){var b=a("."+n.customClass,this).each(function(){c.call(this,!0,"")});setTimeout(function(){b.each(d)},10)})}),a(window).bind("beforeunload.placeholder",function(){var b=!0;try{"javascript:void(0)"===document.activeElement.toString()&&(b=!1)}catch(c){}b&&a("."+n.customClass).each(function(){this.value=""})}))});		
	$('input, textarea').placeholder({customClass:'input-placeholder'});	
}
// ie fix

// placeholder   
$("input, textarea").each(function(){var a=$(this).attr("placeholder");$(this).focus(function(){$(this).attr("placeholder","")}),$(this).focusout(function(){$(this).attr("placeholder",a)})});
// placeholder

// fancybox
var popLen1 = $(".js-popup-1").length;
if(popLen1>0){
	$(".js-popup-1").fancybox({
		autoSize : false,
		fitToView : false,
		maxWidth : '730px',
		width: '100%',
		height: 'auto',
		margin:0,
		padding:0,
		closeSpeed:0					
	})
}

$(".js-skip-button").click(function(){
	$(this).next(".js-skip-link").click();	
	return false;
})

$(".js-close").click(function(){
	$(".fancybox-close").click();	
})

var fancyLen = $(".fancybox").length;
if(fancyLen>0){
	$(".fancybox").fancybox({
	});	
}
// fancybox

// sliders
var sliderLen1 = $('.js-slider-1').length;
if(sliderLen1>0){
	$('.js-slider-1').slick({
	  dots: false,
	  infinite: true,
	  autoplay: true,
	  speed: 1600,
	  autoplaySpeed: 9000,	  
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200,
	  fade:true,
	  adaptiveHeight: true,
	  appendArrows: $(".slider-1__fix")
	});	
}
var sliderLen1 = $('.js-slider-2').length;
if(sliderLen1>0){
	$('.js-slider-2').slick({
	  dots: false,
	  infinite: true,
	  autoplay: false,  
	  slidesToShow: 4,
	  slidesToScroll: 1,
	  touchThreshold: 200,
	  appendArrows: $(".slider-2"),
	  responsive: [
		{breakpoint: 993,settings: {slidesToShow: 3}},
		{breakpoint: 851,settings: {slidesToShow: 2}},
		{breakpoint: 501,settings: {slidesToShow: 1}}
	  ]   
	});	
}
var sliderLen3 = $('.js-slider-3').length;
if(sliderLen3>0){
	$('.js-slider-3').slick({
	  dots: false,
	  infinite: true,
	  autoplay: false,  
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200,
	  centerMode: true,
	  responsive: [
		{breakpoint: 1151,settings: {centerMode: false}}
	  ] 	  
	});	
}

var sliderLen4 = $('.js-slider-4').length;
if(sliderLen4>0){
	$('.js-slider-4').slick({
	  dots: true,
	  infinite: false,
	  autoplay: false,  
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  touchThreshold: 200	  
	});	
	$('.js-slider-4').on('beforeChange', function(event, slick, currentSlide, nextSlide){
		var curInd = nextSlide+1;
		$(".gal-2__item").removeClass("active");
		$(".gal-2__item[data-item="+curInd+"]").addClass("active");	
	});		
	$('.js-slider-4').on('afterChange', function(event, slick, currentSlide, nextSlide){
		$(".gal-2__mask").fadeOut(0);	
	});		
}
// sliders

// ellip
var ellipLen1 = $('.js-ellip-2').length;
if(ellipLen1){
	setTimeout(function(){$('.js-ellip-2').ellipsis({lines: 2,ellipClass: 'ellip',responsive: false});}, 300);	
}
// ellip

// mobile nav
$(".js-mobile-open").click(function(){
	$(".header-nav").stop().slideDown(200);
	$("#mask").stop().fadeIn(200);
})
$(".js-mobile-close").click(function(){$("#mask").click();})
// mobile nav

// fade
$(".js-fade-link").live("click",function(){
	if($(this).hasClass("active")){
		$(this).removeClass("active");
		$(this).closest(".js-fade-wrapper").removeClass("active").find(".js-fade-hide").stop().fadeOut(0);			
	}
	else{
		$(this).addClass("active");
		$(this).closest(".js-fade-wrapper").addClass("active").find(".js-fade-hide").stop().fadeIn(0);		
	}
})

$(".ios .js-fade-link").click(function(e){e.preventDefault();})
// fade

// video
$(".js-video").live("click",function(){
	var videoSrc = $(this).closest(".video").data("frame");
	$(this).closest(".video").append('<iframe class="video__frame" frameborder="0" allowfullscreen></iframe>');
	$(this).closest(".video").find(".video__frame").attr("src",videoSrc).fadeIn(0);	
	$(this).fadeOut(100);
})
// video

// formstyler
var formLen3 = $("select").length;
if(formLen3>0){
	$('select').styler({});
	$(".jq-selectbox__dropdown").each(function(){
        var liLen = $(this).find("li").length;
		if(liLen>17){
			$(this).find(".jq-scroll").mCustomScrollbar({
				horizontalScroll:false,
				scrollButtons:{enable:false},
				advanced:{updateOnContentResize:true},
				advanced:{updateOnBrowserResize:true},
				scrollInertia: 0,
				mouseWheelPixels: 30
			});	
		}
    });
}
// formstyler	

// remove item
$(".js-remove").live("click",function(){
	$(this).closest(".basket-table__row").fadeOut(300,function(){
		$(this).remove();
	})
})
// remove item

// counter
	$('.plus-minus__prev').click(function () {
         var $input = $(this).next();
         var count = parseInt($input.val()) - 1;
         count = count < 1 ? 1 : count;
         $input.val(count);
         $input.change();
         return false;
     });
     $('.plus-minus__next').click(function () {
         var $input = $(this).prev();
         var count = parseInt($input.val()) + 1;
         count = count > ($input.attr("maxlength")) ? ($input.attr("maxlength")) : count;
         $input.val(count);
         $input.change();
         return false;
     });

	$(".plus-minus__input").keypress(function(e){var i,n;if(!e)var e=window.event;return e.keyCode?i=e.keyCode:e.which&&(i=e.which),null==i||0==i||8==i||13==i||9==i||46==i||37==i||39==i?!0:(n=String.fromCharCode(i),/\d/.test(n)?void 0:!1)});			
// counter

// call add popup
$(".product__button").click(function(){
	$(this).next(".js-popup-1").click();	
})
// call add popup

// video
$(".js-video").live("click",function(){
	var videoSrc = $(this).closest(".video").data("frame");
	$(this).closest(".video").append('<iframe class="video__frame" frameborder="0" allowfullscreen></iframe>');
	$(this).closest(".video").find(".video__frame").attr("src",videoSrc).fadeIn(0);	
	$(this).fadeOut(100);
})
// video

// open recipe
$(".js-slide-link").live("click",function(){
	$(this).closest(".js-slide-wrapper").find(".js-slide-hide").slideToggle(300);
	$(this).closest(".js-slide-wrapper").toggleClass("active");	
	$(this).toggleClass("active");	
})
// open recipe
})
/*----------doc ready eof----------*/

/*----------begin window load----------*/
$(window).load(function(){
	// body fix	
	$('body').removeClass('loaded'); 	
	// body fix
})	
/*----------window load eof----------*/

/*----------begin touch----------*/
$(document).on('touchstart', function(){documentClick = true;});
$(document).on('touchmove', function(){documentClick = false;});
$(document).on('click touchend', function(event){	
	if(event.type == "click") documentClick = true;
	if(documentClick){
		var target = $(event.target);
		if(target.is('.js-fade-wrapper *')){return}	
		if(target.is('.js-nav-wrapper *')){return}
		if(target.is('.js-slide-wrapper *')){return}	
		if(target.is('.header-mobile *')){return}	
		if(target.is('#mask')){		
			$(".header-nav").stop().slideUp(200);
			$("#mask").stop().fadeOut(200);			
		}		
		if(target.is('.window__fix')){$(".fancybox-close").click();}
		// gallery
		if(target.is('.js-gal-link')){
			var curItem = $(target).data("item");
			$(".gal-2__mask").fadeIn(0);
			$(".gal-2__item").removeClass("active");
			$(this).addClass("active");
			$(".gal-1 .slick-dots li:nth-child("+curItem+")").click();			
		}	
		// gallery							
		else{
			$(".js-fade-hide").stop().fadeOut(0);$(".js-fade-wrapper, .js-fade-link").removeClass("active");
			$(".js-nav-wrapper").removeClass("active");
			$(".js-nav-hide").stop().fadeOut(200);			
			$(".header-mobile").removeClass("active");
			$("#mask").stop().fadeOut(200);
		}
	}
});
/*----------touch eof----------*/

/*----------begin bind load & resize & orientation eof----------*/	
var handler1 = function(){
// footer fix
setTimeout(function(){
var footH = $(".footer").height();	
$(".main-wrapper").css("padding-bottom",footH+"px");
}, 1);
// footer fix

// ellip
var ellipLen1 = $('.js-ellip-2').length;
if(ellipLen1){
	setTimeout(function(){$('.js-ellip-2').ellipsis({lines: 2,ellipClass: 'ellip',responsive: false});}, 200);	
}
// ellip

// full height
setTimeout(function(){
	$(".js-full-height").css("min-height","0");
	var headH = $(".header").height();
	var footH = $(".footer").height();
	var docH = $(document).height();
	var fixH = docH-(headH+footH);
	$(".js-full-height").css("min-height",fixH+"px");
}, 1);
// full height

// sliders fix
var sliderLen1 = $('.js-slider-1 .slick-track').length;
if(sliderLen1>0){
	$(".js-slider-1").slick('setPosition');	
}
var sliderLen2 = $('.js-slider-2 .slick-track').length;
if(sliderLen2>0){
	$(".js-slider-2").slick('setPosition');	
}
var sliderLen3 = $('.js-slider-3 .slick-track').length;
if(sliderLen3>0){
	$(".js-slider-3").slick('setPosition');	
}
var sliderLen4 = $('.js-slider-4 .slick-track').length;
if(sliderLen4>0){
	$(".js-slider-4").slick('setPosition');	
}
// sliders fix

// equal cols
$('.js-eq-1, .js-eq-2, .js-eq-3').css("min-height","0");
var winW=viewport().width;
$(".js-eq").each(function(){	
	if(winW>992){
		var maxHeight1 = -1; 
		$(this).find('.js-eq-1').each(function() {maxHeight1 = maxHeight1 > $(this).height() ? maxHeight1 : $(this).height();});	   
		$(this).find('.js-eq-1').each(function() {$(this).css("min-height",maxHeight1+"px");}); 
	}
	if(winW<=992){
		var maxHeight1 = -1; 
		$(this).find('.js-eq-2').each(function() {maxHeight1 = maxHeight1 > $(this).height() ? maxHeight1 : $(this).height();});	   
		$(this).find('.js-eq-2').each(function() {$(this).css("min-height",maxHeight1+"px");}); 
		var maxHeight2 = -1; 
		$(this).find('.js-eq-3').each(function() {maxHeight2 = maxHeight2 > $(this).height() ? maxHeight2 : $(this).height();});	   
		$(this).find('.js-eq-3').each(function() {$(this).css("min-height",maxHeight2+"px");}); 		
	}				
})	
// equal cols
}
$(window).bind('orientationchange', handler1);
$(window).bind('resize', handler1);
$(window).bind('load', handler1);
/*----------bind load & resize & orientation eof----------*/

/*----------begin win load----------*/
var handler3 = function(){
// formstyler
var formLen1 = $("input[type=radio]").length;
var formLen2 = $("input[type=checkbox]").length;
if(formLen1>0||formLen2>0){
	$('input[type=checkbox]').styler({});
	$('.radio input[type=radio]').styler({wrapper:'.radio'});
}
// formstyler		
}
$(window).bind('load', handler3);
/*----------win load eof----------*/

/*----------begin bind load & click----------*/
var handler4 = function(){
// formstyler fix
$(".radio__item").each(function(){var a=$(this).find(".jq-radio.checked").length;a>=1?$(this).addClass("active"):$(this).removeClass("active")}),$(".radio__item").each(function(){var a=$(this).find(".jq-radio.disabled").length;a>=1?$(this).addClass("disabled"):$(this).removeClass("disabled")}),$(".checkbox__item").each(function(){var a=$(this).find(".jq-checkbox.checked").length;a>=1?$(this).addClass("active"):$(this).removeClass("active")}),$(".checkbox__item").each(function(){var a=$(this).find(".jq-checkbox.disabled").length;a>=1?$(this).addClass("disabled"):$(this).removeClass("disabled")});	
// formstyler fix		
}
$(window).bind('click', handler4);
$(window).bind('load', handler4);
/*----------bind load & click eof----------*/